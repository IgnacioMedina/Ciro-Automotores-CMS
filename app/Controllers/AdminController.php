<?php


namespace App\Controllers;

use Slim\Views\Twig as View;

use App\Models\Permisos;
use App\Models\Prospecto;
use App\Models\Empleado;
use App\Models\Notificaciones\Notificaciones;

use Illuminate\Database\Capsule\Manager as DB;
use GuzzleHttp\Client;



class AdminController extends Controller {

	public function index($request, $response)
	{
		/*$prospectos = Prospecto::where([['id_vendedor', '=', $_SESSION['userid']], ['estado','=','0']])->orderBy('created_at', 'asc')->get();
		$llamar_tarde = Prospecto::where([['id_vendedor', '=', $_SESSION['userid']], ['estado','=','1']])->get();
		$vendedores = Empleado::where('id_puesto','2')->orderBy('id_agencia','ASC')->get();
		$prospectos_g = Prospecto::orderBy('created_at', 'asc')->get();
 		$puesto = Empleado::where('id_usuario', $_SESSION['userid'])->first();

		return $this->container->view->render($response, 'admin_views/home.twig',[
			'prospectos'=>$prospectos,
			'prospectos_tarde'=>$llamar_tarde,
			'prospectos_g'=>$prospectos_g,
			'vendedores'=>$vendedores,
			'id_usuario'=>$_SESSION['userid'],
			'puesto'=> $puesto->puesto->nombre,
		]);*/

		$empleado = Empleado::where('id_usuario','=',$_SESSION['userid'])->first();
		$flag_msg = $empleado->msg_admin;
		$empleado->msg_admin = 1;
		$empleado->save();
		
		return $this->container->view->render($response, 'admin_views/home.twig', [
			'mostrar_msg' => $flag_msg,
		]);
	}

	public function rendimiento($request, $response)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/b93cd8de78148b43648df1a49c8018aa/settings/development_mode",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: 439e991b-77a4-0837-fa3a-c3afa571c3a1",
		    "x-auth-email: ignaciomedina1@hotmail.com.ar",
		    "x-auth-key: 6baa7ebbb8e343b9fc046e7f4606d18bc4400"
		  ),
		));

		$respuesta = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		return $this->container->view->render($response, 'admin_views/admin/rendimiento.twig', [
			//'estado'=> $estado,
			'respuesta'=> json_decode($respuesta),
		]);
	}

	public function rendimiento_desarrollo($request, $response)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/b93cd8de78148b43648df1a49c8018aa/settings/development_mode",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PATCH",
		  CURLOPT_POSTFIELDS => "{\n\t\"value\":\"on\"\n}",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/json",
		    "x-auth-email: ignaciomedina1@hotmail.com.ar",
		    "x-auth-key: 6baa7ebbb8e343b9fc046e7f4606d18bc4400"
		  ),
		));

		$respuesta = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $respuesta;
		}

		$this->flash->addMessage('info', 'El modo de desarrollo fue activado, se desactivo el caché de Twig y de Cloudflare. <br>Respuesta servidor:<br> <code>' . $respuesta . '</code>');
		$url = $this->router->pathFor('godmode.rendimiento');

		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function rendimiento_desarrollo_desactivar($request, $response)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/b93cd8de78148b43648df1a49c8018aa/settings/development_mode",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PATCH",
		  CURLOPT_POSTFIELDS => "{\n\t\"value\":\"off\"\n}",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/json",
		    "x-auth-email: ignaciomedina1@hotmail.com.ar",
		    "x-auth-key: 6baa7ebbb8e343b9fc046e7f4606d18bc4400"
		  ),
		));

		$respuesta = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $respuesta;
		}

		$this->flash->addMessage('info', 'El modo de desarrollo fue desactivado, se re-activo el caché de Twig y de Cloudflare. <br>Respuesta servidor:<br> <code>' . $respuesta . '</code>');
		$url = $this->router->pathFor('godmode.rendimiento');

		return $response->withStatus(302)->withHeader('Location', $url);
	}
}
