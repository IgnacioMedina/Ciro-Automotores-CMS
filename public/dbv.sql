-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-08-2018 a las 16:20:51
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ciroautomotores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas`
--

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `url_name` varchar(150) NOT NULL,
  `descripcion` text NOT NULL,
  `keywords` text NOT NULL,
  `imagen` text NOT NULL,
  `mostrar` tinyint(1) NOT NULL,
  `eliminado` tinyint(1) NOT NULL DEFAULT '0',
  `contador` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas`
--

INSERT INTO `paginas` (`id`, `id_usuario`, `id_marca`, `titulo`, `url_name`, `descripcion`, `keywords`, `imagen`, `mostrar`, `eliminado`, `contador`, `created_at`, `updated_at`) VALUES
(9, 30, 6, 'Nueva Ford EcoSport', 'nueva-ford-ecosport', 'Elegante y agresiva', 'autos usados salta, nueva ford ecosport', '', 0, 0, 21, '2018-07-30 12:27:17', '2018-08-01 11:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques`
--

CREATE TABLE `paginas_bloques` (
  `id` int(11) NOT NULL,
  `id_pagina` int(11) NOT NULL,
  `nombre` varchar(36) NOT NULL,
  `bloquediv` varchar(36) NOT NULL,
  `href` varchar(96) NOT NULL,
  `orden` int(11) NOT NULL,
  `tipo` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques`
--

INSERT INTO `paginas_bloques` (`id`, `id_pagina`, `nombre`, `bloquediv`, `href`, `orden`, `tipo`, `created_at`, `updated_at`) VALUES
(457, 9, 'Presentación', '', 'presentacion', 1, 'fullscreen', '2018-07-31 11:23:55', '2018-07-31 11:23:55'),
(458, 9, 'Características', '', 'caracteristicas', 2, 'textoimg', '2018-07-31 11:23:55', '2018-07-31 11:23:55'),
(459, 9, 'Galería', '', 'galeria', 3, 'galeria', '2018-07-31 11:23:55', '2018-07-31 11:23:55'),
(460, 9, 'Ficha Técnica', '', 'ficha-tecnica', 4, 'fichatecnica', '2018-07-31 11:23:55', '2018-07-31 11:23:55'),
(461, 9, 'Versiones', '', 'versiones', 5, 'versiones', '2018-07-31 11:23:55', '2018-07-31 11:23:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques_fichatecnica`
--

CREATE TABLE `paginas_bloques_fichatecnica` (
  `id` int(11) NOT NULL,
  `id_bloque` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `titulo` varchar(64) NOT NULL,
  `parrafo` text NOT NULL,
  `id_icono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques_fichatecnica`
--

INSERT INTO `paginas_bloques_fichatecnica` (`id`, `id_bloque`, `orden`, `titulo`, `parrafo`, `id_icono`) VALUES
(83, 460, 1, 'Seguridad', '<p>Lorem ipsum dolor sit amet consectetur adipiscing elit auctor vel, sapien aliquet ultrices tincidunt eros dignissim turpis potenti metus, posuere sociis et hac odio vulputate curabitur sagittis. Pharetra dictumst conubia cum cursus ut quis egestas congue elementum, velit tempor class euismod parturient aenean nibh ac, molestie ullamcorper commodo porttitor consequat praesent fusce vivamus. Ante orci eleifend auctor integer eros felis, dignissim rhoncus donec etiam blandit tincidunt vulputate, sed purus nibh tempus dapibus.</p>', 10),
(84, 460, 2, 'Dimensiones', '<p>Porta vivamus suscipit natoque venenatis quisque, conubia molestie pellentesque per dictum placerat, curae vitae tristique netus. Laoreet sem viverra praesent netus integer primis tellus penatibus blandit magnis in cursus congue, aliquam elementum posuere ad habitant vitae etiam ornare erat dapibus duis semper. Ridiculus blandit lectus volutpat quis tempus ligula sociis vehicula congue posuere, hac eleifend nascetur per platea ad euismod primis facilisi, nullam malesuada nulla nostra porta lacinia proin scelerisque viverra.</p>', 3),
(85, 460, 3, 'Frenos', '<p>Bibendum sagittis proin fermentum hendrerit porta dignissim ornare etiam, ullamcorper porttitor lobortis feugiat purus cursus dis, sociis ut maecenas curabitur vestibulum ad aliquam. Ligula aliquet habitasse potenti dis tempor scelerisque dictumst mattis gravida, massa laoreet tincidunt orci consequat senectus placerat nisi accumsan leo, inceptos facilisi parturient natoque felis ullamcorper maecenas fusce. Rutrum magna ad orci egestas porta maecenas habitant rhoncus, natoque cras ultrices tortor purus tincidunt eleifend gravida dapibus, pellentesque penatibus dignissim hac scelerisque vehicula mus.</p>', 4),
(86, 460, 4, 'Puertas', '<p>Odio laoreet natoque pretium libero id est montes, metus ut gravida rhoncus eu arcu viverra, consequat tempor habitasse cursus class senectus. Sociis dictum porta convallis metus tempus urna vitae id est, duis nunc mattis massa ultricies rhoncus a nec facilisi libero, sed feugiat curae natoque sem fermentum non laoreet. Hendrerit placerat tempor himenaeos neque enim lacus viverra, ultrices curabitur sem leo cubilia imperdiet vitae vel, vivamus fames mauris augue malesuada volutpat.</p>', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques_fotogaleria`
--

CREATE TABLE `paginas_bloques_fotogaleria` (
  `id` int(11) NOT NULL,
  `id_bloque` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `enlace` text NOT NULL,
  `mode` int(11) NOT NULL,
  `alt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques_fotogaleria`
--

INSERT INTO `paginas_bloques_fotogaleria` (`id`, `id_bloque`, `orden`, `enlace`, `mode`, `alt`) VALUES
(439, 459, 1, '/images/uploads/nueva-ford-ecosport-galer--a_9_459_1.jpg', 0, ''),
(440, 459, 2, '/images/uploads/nueva-ford-ecosport-galer--a_9_459_2.jpg', 0, ''),
(441, 459, 3, '/images/uploads/nueva-ford-ecosport-galer--a_9_459_3.jpg', 0, ''),
(442, 459, 4, '/images/uploads/nueva-ford-ecosport-galer--a_9_459_4.jpg', 0, ''),
(443, 459, 5, '/images/uploads/nueva-ford-ecosport-galer--a_9_459_5.jpg', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques_fullscreen`
--

CREATE TABLE `paginas_bloques_fullscreen` (
  `id` int(11) NOT NULL,
  `id_bloque` int(11) NOT NULL,
  `titulo` varchar(128) NOT NULL,
  `subtitulo` varchar(128) NOT NULL,
  `imagen` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques_fullscreen`
--

INSERT INTO `paginas_bloques_fullscreen` (`id`, `id_bloque`, `titulo`, `subtitulo`, `imagen`) VALUES
(108, 457, 'Nueva Ford EcoSport', 'Elegante y agresiva', '/images/uploads/nueva-ford-ecosport-presentaci--n_9_457_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques_textoimg`
--

CREATE TABLE `paginas_bloques_textoimg` (
  `id` int(11) NOT NULL,
  `id_bloque` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `parrafo` text NOT NULL,
  `imagen` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques_textoimg`
--

INSERT INTO `paginas_bloques_textoimg` (`id`, `id_bloque`, `orden`, `titulo`, `parrafo`, `imagen`, `created_at`, `updated_at`) VALUES
(352, 458, 1, 'Nueva Ford EcoSport Titanium ', '<p>Con un dise&ntilde;o&nbsp;interesante, imponente y solido,&nbsp;destacado por unos guardafangos voluminosos,&nbsp;<strong>La Nueva Ford EcoSport Titanium,</strong>inconfundible miembro de la&nbsp;<strong>familia Ford,</strong>&nbsp;se re- estiliza para mantener su posici&oacute;n de privilegio en el mercado automotriz, resaltando la identidad deportiva de esta gama.&nbsp;Siendo el&nbsp;<strong>SUV&nbsp;</strong>m&aacute;s peque&ntilde;o de la marca mantiene su robustez con l&iacute;neas fuertes y marcadas, es ahora m&aacute;s atractiva que nunca, con un frontal similar al de&nbsp;<strong>la Ford Kuga</strong><strong>EcoSport&nbsp;</strong>y en la parte posterior mantiene la caracter&iacute;stica compuerta de apertura lateral, con una excelente habitabilidad y una imagen m&aacute;s &ldquo;offroad&rdquo;, la&nbsp;<strong>EcoSport l&iacute;nea nueva</strong>, es la opci&oacute;n m&aacute;s importante&nbsp;si buscas una camioneta para tu uso constante del d&iacute;a a d&iacute;a.</p>\n', '/images/uploads/nueva-ford-ecosport-caracter--sticas_9_458_1.jpg', '2018-07-31 11:23:55', '2018-07-31 11:23:55'),
(353, 458, 2, '', '<p>De silueta moderna, frontal muy agresivo y con medidas exteriores compactas, la&nbsp;<strong>nueva EcoSport Ford Titanium&nbsp;</strong>proporciona un esp&iacute;ritu urbano, adem&aacute;s del estilo de un&nbsp;<strong>todo terreno,</strong>&nbsp;gracias a su carrocer&iacute;a elevada con 200 mil&iacute;metros con respecto al suelo, lo que le permite una capacidad de vadeo de 550 mil&iacute;metros de profundidad, con &aacute;ngulo de ataque de 35 grados y un &aacute;ngulo de salida de 21 grados, que favorecen al momento de afrontar pendientes muy pronunciadas. La rueda de auxilio&nbsp;se mantiene insertada en la compuerta trasera de apertura lateral, detalle que siempre ha sido caracter&iacute;stico y propio de este modelo. Es un deportivo tecnol&oacute;gico, preparado para el asfalto y tambi&eacute;n para salir de terrenos dif&iacute;ciles para cualquier otro automovil urbano, es un veh&iacute;culo din&aacute;mico que combina&nbsp;<strong>deportividad y confort</strong>&nbsp;a los mismos niveles, capaz de llegar a los lugares m&aacute;s rec&oacute;nditos sin que nada se interponga en su camino.</p>\n', '/images/uploads/nueva-ford-ecosport-caracter--sticas_9_458_2.jpg', '2018-07-31 11:23:55', '2018-07-31 11:23:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_bloques_versiones`
--

CREATE TABLE `paginas_bloques_versiones` (
  `id` int(11) NOT NULL,
  `id_bloque` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `contenido` text NOT NULL,
  `enlace` text NOT NULL,
  `orden` int(11) NOT NULL,
  `predeterminado` varchar(11) NOT NULL,
  `id_vehiculo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_bloques_versiones`
--

INSERT INTO `paginas_bloques_versiones` (`id`, `id_bloque`, `titulo`, `contenido`, `enlace`, `orden`, `predeterminado`, `id_vehiculo`) VALUES
(49, 461, 'Nueva Ford EcoSport S', '<p>qweqweqwe</p>', '/images/uploads/nueva-ford-ecosport-versiones_9_461_1.jpg', 1, '', 3687),
(50, 461, 'Nueva Ford EcoSport SE', '<p>qweqwe</p>\n', '/images/uploads/nueva-ford-ecosport-versiones_9_461_2.jpg', 2, '', 3688);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas_iconos`
--

CREATE TABLE `paginas_iconos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `enlace` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paginas_iconos`
--

INSERT INTO `paginas_iconos` (`id`, `id_usuario`, `nombre`, `enlace`, `created_at`, `updated_at`) VALUES
(1, 30, 'Cilindrada', '/images/iconos/cilindrada.png', '2018-07-16 11:55:17', '2018-07-16 11:55:17'),
(2, 30, 'Color', '/images/iconos/color.png', '2018-07-16 11:55:31', '2018-07-16 11:55:31'),
(3, 30, 'Dimensiones', '/images/iconos/dimensiones.png', '2018-07-16 11:55:49', '2018-07-16 11:55:49'),
(4, 30, 'Freno', '/images/iconos/freno.png', '2018-07-16 11:55:54', '2018-07-16 11:55:54'),
(5, 30, 'Kilometraje', '/images/iconos/kilometraje.png', '2018-07-16 11:55:59', '2018-07-16 11:55:59'),
(6, 30, 'Motor', '/images/iconos/motor.png', '2018-07-16 11:56:08', '2018-07-16 11:56:08'),
(7, 30, 'Prestaciones', '/images/iconos/prestaciones.png', '2018-07-16 11:56:15', '2018-07-16 11:56:15'),
(8, 30, 'Puertas', '/images/iconos/puertas.png', '2018-07-16 11:56:20', '2018-07-16 11:56:20'),
(9, 30, 'Rodado', '/images/iconos/rodado.png', '2018-07-16 11:56:24', '2018-07-16 11:56:24'),
(10, 30, 'Seguridad', '/images/iconos/seguridad.png', '2018-07-16 11:56:29', '2018-07-16 11:56:29'),
(11, 30, 'Suspensión', '/images/iconos/suspensi--n.png', '2018-07-16 11:56:38', '2018-07-16 11:56:38'),
(12, 30, 'Transmisión', '/images/iconos/transmisi--n.png', '2018-07-16 11:56:48', '2018-07-16 11:56:48'),
(13, 30, 'Year', '/images/iconos/year.png', '2018-07-16 11:56:54', '2018-07-16 11:56:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_bloques`
--
ALTER TABLE `paginas_bloques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `paginas_bloques_fichatecnica`
--
ALTER TABLE `paginas_bloques_fichatecnica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_bloques_fotogaleria`
--
ALTER TABLE `paginas_bloques_fotogaleria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_bloques_fullscreen`
--
ALTER TABLE `paginas_bloques_fullscreen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_bloques_textoimg`
--
ALTER TABLE `paginas_bloques_textoimg`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_bloques_versiones`
--
ALTER TABLE `paginas_bloques_versiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paginas_iconos`
--
ALTER TABLE `paginas_iconos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `paginas`
--
ALTER TABLE `paginas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques`
--
ALTER TABLE `paginas_bloques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques_fichatecnica`
--
ALTER TABLE `paginas_bloques_fichatecnica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques_fotogaleria`
--
ALTER TABLE `paginas_bloques_fotogaleria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=444;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques_fullscreen`
--
ALTER TABLE `paginas_bloques_fullscreen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques_textoimg`
--
ALTER TABLE `paginas_bloques_textoimg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT de la tabla `paginas_bloques_versiones`
--
ALTER TABLE `paginas_bloques_versiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `paginas_iconos`
--
ALTER TABLE `paginas_iconos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
