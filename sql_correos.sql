/*
CREATE TABLE `ciroautomotores`.`modulo_correo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `id_enlace` INT NOT NULL,
  `url_name` VARCHAR(128) NOT NULL,
  `email` VARCHAR(128) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`publicacion_vista` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_publicacion` INT NOT NULL,
  `ip` VARCHAR(48) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`usuario_actividad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_session` INT NOT NULL,
  `route` VARCHAR(256),
  `method` VARCHAR(16) NOT NULL,
  `status_code` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
CHANGE COLUMN `estado` `estado` TINYINT NOT NULL DEFAULT 1 ;

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
CHANGE COLUMN `tipo_cuenta` `tipo_cuenta` INT(11) NOT NULL COMMENT '1 cliente - 2 proveedor - 3 banco' ;

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
ADD COLUMN `id_sucursal` INT NOT NULL AFTER `id`;

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
CHANGE COLUMN `id_provedor-cliente` `id_provedor_cliente` INT(11) NOT NULL ;


ALTER TABLE `ciroautomotores`.`usuario_session`
ADD COLUMN `device_type` INT NOT NULL DEFAULT 0 COMMENT '0 = iOS\n1 = ANDROID\n2 = AMAZON\n3 = WINDOWSPHONE (MPNS)\n4 = CHROME APPS / EXTENSIONS\n5 = CHROME WEB PUSH\n6 = WINDOWS (WNS)\n7 = SAFARI\n8 = FIREFOX\n9 = MACOS\n10 = ALEXA\n11 = EMAIL' AFTER `estado`,
ADD COLUMN `os_id` VARCHAR(32) NOT NULL AFTER `device_type`;

INSERT INTO `ciroautomotores`.`tra_tipos_movimiento` (`id`, `nombre`, `estado`) VALUES ('6', 'transferencia', '1');

CREATE TABLE `ciroautomotores`.`tra_sucursal_transferencia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_sucursal_origen` INT NOT NULL,
  `id_sucursal_destino` INT NOT NULL,
  `id_comprobante_origen` INT NOT NULL,
  `id_comprobante_destino` INT NOT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  `responsable` VARCHAR(128) NOT NULL
  `observaciones` VARCHAR(256) NOT NULL,
  `id_usuario` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));
*/

/* SISTEMAS DE CARPETAS
CREATE TABLE `ciroautomotores`.`carpetas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `operacion` VARCHAR(16) NOT NULL DEFAULT 0,
  `id_usuario` INT NOT NULL,
  `id_sucursal` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`carpeta_cliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_cliente` INT NOT NULL,
  `id_carpeta` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`carpeta_vehiculo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_vehiculo` INT NOT NULL,
  `id_carpeta` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`dateros`
ADD COLUMN `id_carpeta` INT NOT NULL DEFAULT 0 AFTER `id_usuario`;

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
ADD COLUMN `id_carpeta` INT NOT NULL DEFAULT 0 AFTER `updated_at`;

ALTER TABLE `ciroautomotores`.`carpetas`
ADD COLUMN `id_cliente` INT NOT NULL DEFAULT 0 AFTER `id_sucursal`,
ADD COLUMN `id_vehiculo` INT NOT NULL DEFAULT 0 AFTER `id_cliente`,
ADD COLUMN `id_datero` INT NOT NULL DEFAULT 0 AFTER `id_vehiculo`,
ADD COLUMN `id_cuentacorriente` INT NOT NULL DEFAULT 0 AFTER `id_datero`,
ADD COLUMN `aprobado` DATETIME NULL AFTER `id_cuentacorriente`,
ADD COLUMN `cerrado` DATETIME NULL AFTER `aprobado`;

ALTER TABLE `ciroautomotores`.`tra_cuentacorriente`
CHANGE COLUMN `id_vehiculo` `id_vehiculo` INT(11) NOT NULL DEFAULT 0 ;

CREATE TABLE `ciroautomotores`.`datero_tipos_estado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(64) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`datero_historial` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_datero` INT NOT NULL,
  `id_usuario` INT NOT NULL DEFAULT 0,
  `id_tipo_estado` INT NOT NULL,
  `descripcion` VARCHAR(128) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`dateros`
ADD COLUMN `id_cliente_conyugue` INT NOT NULL DEFAULT 0 AFTER `id_carpeta`,
ADD COLUMN `id_tipo_estado` INT NOT NULL DEFAULT 1 AFTER `id_cliente_conyugue`;

INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('nuevo', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('enviado', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('modificado', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('visto', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('revision', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('pre-aprobado', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('aprobado', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('rechazado', '1');
INSERT INTO `ciroautomotores`.`datero_tipos_estado` (`nombre`, `estado`) VALUES ('cancelado', '1');

INSERT INTO `ciroautomotores`.`prospecto_tipo_estados` (`id`, `nombre`, `estado`) VALUES ('7', 'Revision', '1');
ALTER TABLE `ciroautomotores`.`usados`
ADD COLUMN `id_cliente` INT NOT NULL DEFAULT 0 AFTER `updated_at`;

CREATE TABLE `ciroautomotores`.`vehiculo_tipos_formulario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NOT NULL,
  `caducidad` INT NOT NULL DEFAULT 0,
  `arancel` DECIMAL(10,2) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`vehiculo_formulario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_vehiculo` INT NOT NULL,
  `id_formulario` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `fecha_vto` DATE NULL,
  `responsable` VARCHAR(128) NOT NULL,
  `observaciones` VARCHAR(128) NOT NULL,
  `arancel` DECIMAL(10,2) NOT NULL,
  `orden` INT NOT NULL DEFAULT 1,
  `tipo` INT NOT NULL DEFAULT 0 COMMENT '0 TODOS\n1 USADOS\n2 NUEVOS',
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Titulo', '', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Cedula', '365', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('CEDULA AZUL', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F12', 'Verificacion de chasis y motor en gendarmeria', '150', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F13 I', 'Formulario de informe, multas.', '30', '300', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F02', 'Informe de dominio', '30', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F08', 'Transferencia', '30', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('DDJJ', 'Declaracion jurada', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Libre deuda y Baja', 'Libre de deuda y baja municipal', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Revision', 'REVESA', '0', '100', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Prenda', 'Contrato Prendario', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F03', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `descripcion`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('F01', 'Formulario de inscripcion', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Certificado de Fabrica', '0', '0', '1', 'NOW()', 'NOW()');
  INSERT INTO `ciroautomotores`.`vehiculo_tipos_formulario` (`nombre`, `caducidad`, `arancel`, `estado`, `created_at`, `updated_at`) VALUES ('Factura', '0', '0', '1', 'NOW()', 'NOW()');

  UPDATE `ciroautomotores`.`datero_tipos_estado` SET `estado`='0' WHERE `id`='1';
  UPDATE `ciroautomotores`.`datero_tipos_estado` SET `estado`='0' WHERE `id`='2';
  UPDATE `ciroautomotores`.`datero_tipos_estado` SET `estado`='0' WHERE `id`='3';
  UPDATE `ciroautomotores`.`datero_tipos_estado` SET `estado`='0' WHERE `id`='4';

*/

ALTER TABLE `ciroautomotores`.`notificaciones`
ADD COLUMN `titulo` VARCHAR(64) NOT NULL AFTER `categoria`,
ADD COLUMN `url` VARCHAR(128) NULL AFTER `titulo`;

ALTER TABLE `ciroautomotores`.`notificaciones`
CHANGE COLUMN `estado` `estado` TINYINT NOT NULL DEFAULT 1 AFTER `updated_at`,
CHANGE COLUMN `categoria` `id_puesto` INT(11) NOT NULL DEFAULT 0 ;

ALTER TABLE `ciroautomotores`.`notificaciones`
CHANGE COLUMN `id_usuario` `id_usuario` INT(11) NOT NULL DEFAULT 0 ;

CREATE TABLE `ciroautomotores`.`usuario_terminal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL DEFAULT 0,
  `latitud` DECIMAL(10,8) NOT NULL DEFAULT 0,
  `longitud` DECIMAL(11,8) NOT NULL DEFAULT 0,
  `os_user_id` VARCHAR(64) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

  ALTER TABLE `ciroautomotores`.`usuario_session`
CHANGE COLUMN `os_id` `id_terminal` INT NOT NULL DEFAULT 0 ;
