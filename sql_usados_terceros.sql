/*
CREATE TABLE `ciroautomotores`.`publicacion_renobacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL COMMENT 'Usuario que dio autorizacion a la publicacion\n',
  `id_publicacion` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `dias` INT NOT NULL,
  `fecha_vto` DATE NOT NULL,
  `observacion` VARCHAR(256) NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `id_publicacion_idx` (`id_publicacion` ASC),
  CONSTRAINT `fk_publicacion`
    FOREIGN KEY (`id_publicacion`)
    REFERENCES `ciroautomotores`.`publicaciones` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `ciroautomotores`.`veh_tipos_imagen` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(64) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`veh_imagenes` 
CHANGE COLUMN `estado` `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
ADD COLUMN `id_tipo_imagen` INT NOT NULL DEFAULT 1 AFTER `id_usuario`,
ADD COLUMN `orden` INT NOT NULL DEFAULT 0 COMMENT 'Orden en que aparecen en el listado para mostrar' AFTER `archivo`;

INSERT INTO `ciroautomotores`.`veh_tipos_imagen` (`nombre`) VALUES ('varios');
INSERT INTO `ciroautomotores`.`veh_tipos_imagen` (`nombre`) VALUES ('frente');
INSERT INTO `ciroautomotores`.`veh_tipos_imagen` (`nombre`) VALUES ('frontal izquierdo');
INSERT INTO `ciroautomotores`.`veh_tipos_imagen` (`nombre`) VALUES ('frontal derecho');
INSERT INTO `ciroautomotores`.`veh_tipos_imagen` (`nombre`) VALUES ('atras');

//////////////////////////////////////////

/*
ALTER TABLE `ciroautomotores`.`suscripciones` 
ADD COLUMN `origen` VARCHAR(255) NOT NULL AFTER `email`,
ADD COLUMN `token` VARCHAR(255) NOT NULL AFTER `origen`;
ADD COLUMN `gr_contactId` VARCHAR(45) NOT NULL AFTER `id`;

/*
CREATE TABLE `ciroautomotores`.`suscripcion_campaña` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_suscripcion` INT NOT NULL,
  `gr_campaignId` VARCHAR(45) NOT NULL,
  `gr_name` VARCHAR(45) NOT NULL,
  `gr_estado` TINYINT NOT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));
  

ALTER TABLE `ciroautomotores`.`consultas` 
ADD COLUMN `estado` TINYINT NOT NULL DEFAULT 1 AFTER `updated_at`;

ALTER TABLE `ciroautomotores`.`individuos` 
CHANGE COLUMN `tipo_dni` `id_tipo_documento` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `dni` `documento` BIGINT(20) NULL DEFAULT NULL ;

ALTER TABLE `ciroautomotores`.`individuos` 
ADD COLUMN `email` VARCHAR(128) NULL AFTER `domicilio`;

ALTER TABLE `ciroautomotores`.`tra_comprobantes` 
ADD COLUMN `cae` VARCHAR(16) NULL AFTER `numero`;
ALTER TABLE `ciroautomotores`.`tra_comprobantes` 
ADD COLUMN `cae_vto` DATE NULL AFTER `cae`;


ALTER TABLE `ciroautomotores`.`tra_comprobantes` 
ADD COLUMN `nogravado` DECIMAL(10,2) NOT NULL AFTER `impuesto`,
ADD COLUMN `excento` DECIMAL(10,2) NOT NULL AFTER `nogravado`;

ALTER TABLE `ciroautomotores`.`tra_comprobante_detalles` 
ADD COLUMN `impuesto` DECIMAL(10,2) NOT NULL AFTER `cantidad`;

ALTER TABLE `ciroautomotores`.`tra_comprobantes` 
DROP COLUMN `punto_venta`;

CREATE TABLE `ciroautomotores`.`usuario_session` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `ip` VARCHAR(48) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`tra_comprobante_errores` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_comprobante` INT NOT NULL,
  `descripcion` VARCHAR(256) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`tra_comprobantes` 
ADD COLUMN `produccion` TINYINT NOT NULL DEFAULT 0 AFTER `tipo`,
ADD COLUMN `reproceso` TINYINT NOT NULL DEFAULT 0 AFTER `produccion`;

ALTER TABLE `ciroautomotores`.`tra_comprobante_errores` 
ADD COLUMN `id_usuario` INT NULL AFTER `id_comprobante`;

ALTER TABLE `ciroautomotores`.`tra_proveedores` 
ADD COLUMN `id_tipo_responsable` INT NOT NULL DEFAULT 0 AFTER `id_tipo_documento`;

*/
/*
ALTER TABLE `ciroautomotores`.`tra_obligaciones` 
DROP COLUMN `documento`,
DROP COLUMN `cheque`,
CHANGE COLUMN `id_cancelo` `id_usuario` INT(11) NULL ,
ADD COLUMN `id_movimiento` INT NOT NULL AFTER `id_sucursal`;

ALTER TABLE `ciroautomotores`.`tra_obligaciones` 
CHANGE COLUMN `fecha_inicio` `fecha_inicio` DATE NOT NULL ,
CHANGE COLUMN `fecha_aproxfin` `fecha_aproxfin` DATE NOT NULL ,
ADD COLUMN `emisor` VARCHAR(128) NOT NULL AFTER `id_movimiento`,
ADD COLUMN `receptor` VARCHAR(128) NOT NULL AFTER `emisor`,
ADD COLUMN `importe` DECIMAL NOT NULL AFTER `receptor`;

ALTER TABLE `ciroautomotores`.`tra_movimientos` 
ADD COLUMN `tarjeta_adicional` DECIMAL(10,2) NULL DEFAULT NULL AFTER `tarjeta_cupon`,
ADD COLUMN `tarjeta_cuota` INT NULL DEFAULT NULL AFTER `tarjeta_adicional`;
*/

CREATE TABLE `ciroautomotores`.`suscripcion_lista` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_suscripcion` INT NOT NULL,
  `mc_memberId` VARCHAR(64) NOT NULL,
  `mc_listId` VARCHAR(16) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));


ALTER TABLE `ciroautomotores`.`tra_diarias` 
DROP COLUMN `id_cancelo`,
DROP COLUMN `fecha_asignacion_diaria`,
CHANGE COLUMN `fecha_inicio_diaria` `fecha_inicio` DATETIME NOT NULL ,
CHANGE COLUMN `fecha_cierre_diaria` `fecha_cierre` DATETIME NULL DEFAULT NULL ,
CHANGE COLUMN `importe_saldoanterior` `saldo_anterior` DECIMAL(10,2) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `importe_totalingreso` `total_ingreso` DECIMAL(10,2) NULL DEFAULT 0 ,
CHANGE COLUMN `importe_totalegreso` `total_egreso` DECIMAL(10,2) NULL DEFAULT 0 ,
CHANGE COLUMN `id_empleado` `id_usuario` INT(11) NOT NULL DEFAULT 0 COMMENT 'Empleado quien dio de alta' ,
CHANGE COLUMN `estado_diaria` `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
ADD COLUMN `saldo` DECIMAL(10,2) NULL DEFAULT 0 AFTER `total_egreso`;

ALTER TABLE `ciroautomotores`.`tra_sucursales` 
ADD COLUMN `estado` TINYINT NOT NULL DEFAULT 1 AFTER `descripcion`,
ADD COLUMN `punto_venta` INT NULL AFTER `estado`;


ALTER TABLE `ciroautomotores`.`usuarios` 
ADD COLUMN `token` VARCHAR(128) NULL AFTER `updated_at`,
ADD COLUMN `estado` TINYINT NOT NULL DEFAULT 1 AFTER `token`;

ALTER TABLE `ciroautomotores`.`usados` 
ADD COLUMN `email` VARCHAR(128) NULL AFTER `exowner`,
ADD COLUMN `id_tipo_documento` INT NULL AFTER `email`,
ADD COLUMN `documento` BIGINT NULL AFTER `id_tipo_documento`,
ADD COLUMN `telefono` VARCHAR(32) NULL AFTER `documento`,
ADD COLUMN `celular` VARCHAR(32) NULL AFTER `telefono`,
ADD COLUMN `domicilio` VARCHAR(128) NULL AFTER `celular`,
ADD COLUMN `numero` VARCHAR(16) NULL AFTER `domicilio`,
ADD COLUMN `piso` VARCHAR(16) NULL AFTER `numero`,
ADD COLUMN `dpto` VARCHAR(16) NULL AFTER `piso`,
ADD COLUMN `id_provincia` INT NULL AFTER `dpto`,
ADD COLUMN `localidad` VARCHAR(64) NULL AFTER `id_provincia`,
ADD COLUMN `cp` INT NULL AFTER `localidad`,
ADD COLUMN `domicilio_observaciones` VARCHAR(256) NULL AFTER `cp`,
ADD COLUMN `id_proveedor` INT NULL AFTER `domicilio_observaciones`,
DROP PRIMARY KEY,
ADD PRIMARY KEY USING BTREE (`dominio`, `id_vehiculo`);

ALTER TABLE `ciroautomotores`.`usados` 
ADD COLUMN `fecha_nacimiento` DATE NULL AFTER `id_proveedor`,
ADD COLUMN `sexo` TINYINT NULL AFTER `fecha_nacimiento`;

CREATE TABLE `ciroautomotores`.`tra_tipos_transaccion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(64) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));

ALTER TABLE `ciroautomotores`.`tra_proveedores` 
ADD COLUMN `id_tipo_proveedor` INT NOT NULL DEFAULT 1 AFTER `id_sucursal`;

INSERT INTO `ciroautomotores`.`tra_tipos_proveedor` (`id`, `nombre`, `estado`) VALUES ('1', 'Comun', '1');
INSERT INTO `ciroautomotores`.`tra_tipos_proveedor` (`id`, `nombre`, `estado`) VALUES ('2', 'Concesionario', '1');

ALTER TABLE `ciroautomotores`.`tra_obligaciones` 
CHANGE COLUMN `importe` `importe` DECIMAL(10,2) NOT NULL ;
