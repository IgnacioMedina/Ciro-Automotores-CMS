CREATE TABLE `ciroautomotores`.`vir_dominios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(64) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `ciroautomotores`.`vir_usuarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_dominio` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `contraseña` VARCHAR(128) NOT NULL,
  `email` VARCHAR(128) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));

CREATE TABLE `ciroautomotores`.`vir_alias` (
  `id` INT NOT NULL,
  `id_dominio` INT NOT NULL,
  `origen` VARCHAR(128) NOT NULL,
  `destino` VARCHAR(128) NOT NULL,
  `estado` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_dominio_idx` (`id_dominio` ASC),
  CONSTRAINT `fk_dominio`
    FOREIGN KEY (`id_dominio`)
    REFERENCES `ciroautomotores`.`vir_dominios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `ciroautomotores`.`vir_alias` 
DROP INDEX `fk_dominio_idx` ,
ADD INDEX `fk_alias_dominio_idx` (`id_dominio` ASC);
